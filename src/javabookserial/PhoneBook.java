/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabookserial;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author human
 */
public interface PhoneBook extends Serializable {

    /**
     * Creates new contact by arguments and return contact id.
     *
     * @param fullName Full name.
     * @param phoneNumber Phone number.
     * @param emailAddress E-mail address.
     * @param skype Skype nickname.
     * @return generated contact id.
     */
    long addContact(String fullName, String phoneNumber, String emailAddress, String skype);

    Contact getById(long id);

    List<Contact> findByName(String name);

    Contact removeContact(long id);
}
