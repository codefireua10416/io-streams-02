/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabookserial;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Яна
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ContactBook contactBook = null;

        System.out.println("Loading contacts...");
        try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream("phonebook.bin"))) {
            contactBook = (ContactBook) oos.readObject();
            System.out.printf("Loaded %d contacts\n", contactBook.getContactList().size());
        } catch (IOException | ClassNotFoundException ex) {
            contactBook = new ContactBook();
            System.out.println("Phone book file is corrupt.");
        }

//        contactBook.addContact("Pupkin Vasya", "+38 (091) 123-45-67", "v.pupkin@gmail.com", null);
//        contactBook.addContact("Pupkin Petya", "+38 (091) 123-54-67", "p.pupkin@gmail.com", "pupkov");
//        contactBook.addContact("Pupkin Senya", "+38 (091) 321-00-12", "s.pupkin@gmail.com", null);
//        for (Contact contact : contactBook) {
//            System.out.println(contact);
//        }
        Scanner scanner = new Scanner(System.in);

        boolean working = true, changes = false;

        while (working) {
            System.out.println("ACTIONS");
            System.out.println("1. [A]dd new contact");
            System.out.println("2. [S]how all contacts");
            System.out.println("3. [G]et contact by id");
            System.out.println("4. [D]elete contact by id");
            System.out.println("0. [E]xit");

            String action = scanner.nextLine();
            String input = scanner.nextLine();

            switch (action.toLowerCase()) {
                case "1":
                case "a":
                    String fullName = input(scanner, "* Enter full name (min 3 symbols): ", true);
                    String phoneNumber = input(scanner, "* Enter phone number (+381234567): ", true);
                    String emailAddress = input(scanner, "Enter e-mail address: ", false);
                    String skype = input(scanner, "Enter skype: ", false);

                    try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("phonebook.bin"))) {

                        oos.writeObject(input);

                    } catch (IOException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    contactBook.addContact(fullName, phoneNumber, emailAddress, skype);
                    changes = true;
                    // TODO: Implement option.
                    break;
                case "2":
                case "s":
                    // TODO: Implement option.

                    try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream("phonebook.bin"))) {
                        contactBook = (ContactBook) oos.readObject();
                        System.out.println( contactBook.getContactList());
                    } catch (IOException | ClassNotFoundException ex) {
                        contactBook = new ContactBook();
                    }

                    break;
                case "3":
                case "g":
                    // TODO: Implement option.

                    String id= input(scanner, "* Enter contact ID: ", true);

                    String contact = contactBook.getById(id);

                    try (ObjectInputStream oos = new ObjectInputStream(new FileInputStream("phonebook.bin"))) {
                        contactBook = (ContactBook) oos.readObject();
                        System.out.printf("You searching %d contact \n", contactBook.getById(id));
                    } catch (IOException | ClassNotFoundException ex) {
                        contactBook = new ContactBook();

                    }

                    break;
                case "4":
                case "d":
                    changes = true;
                    String id2 = input(scanner, "* Enter contact ID that you want to remove? : ", true);
                    // TODO: Implement option.
                    break;
                case "0":
                case "e":
                    working = false;
                    continue;
                default:
                    System.out.println("Wrong action: " + action);
                    break;
            }
        }

        if (changes) {
            System.out.println("Save changes? (y/N)");
            String choose = scanner.nextLine();

            if (choose.toLowerCase().startsWith("y")) {
                try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("phonebook.bin"))) {
                    oos.writeObject(contactBook);
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
    

    

    

    private static String input(Scanner scanner, String prompt, Boolean required) {
        if (required) {
            while (true) {
                System.out.print(prompt);
                String input = scanner.nextLine();

                if (!input.isEmpty()) {
                    return input;
                }
            }
        } else {
            System.out.print(prompt);
            String input = scanner.nextLine();

            return input;

        }

    }

}
