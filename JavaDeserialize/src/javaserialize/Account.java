/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaserialize;

import java.io.Serializable;

/**
 *
 * @author human
 */
public class Account implements Serializable {

    private String address;
    private String username;
    private String password;

    public Account(String address, String username, String password) {
        this.address = address;
        this.username = username;
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
