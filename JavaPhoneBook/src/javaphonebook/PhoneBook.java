/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaphonebook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class PhoneBook {

    private File phoneDir;

    public PhoneBook(String directory) {
        phoneDir = new File(directory);
    }

    public void addContact(String... data) {
        if (!phoneDir.exists()) {
            phoneDir.mkdirs();
        }

        String filename = String.format("%d.txt", System.currentTimeMillis());

        File contactFile = new File(phoneDir, filename);

        try (FileOutputStream fos = new FileOutputStream(contactFile)) {
            for (int i = 0; i < data.length; i++) {
                fos.write(data[i].getBytes());

                if (i < data.length - 1) {
                    fos.write("\n".getBytes());
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(PhoneBook.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<String> getContactList() {
        List<String> nameList = new ArrayList<>();

        File[] listFiles = phoneDir.listFiles();

        if (listFiles != null) {
            for (File contactFile : listFiles) {
                String number = contactFile.getName().replace(".txt", "");

                try (Scanner scanner = new Scanner(new FileInputStream(contactFile))) {
                    String fullName = scanner.nextLine();
                    nameList.add(String.format("%s: (%s)", number, fullName));
                } catch (IOException ex) {
                    Logger.getLogger(PhoneBook.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return nameList;
    }

    public String getContact(String contact_id) {
        File contactFile = new File(phoneDir, contact_id + ".txt");

        if (contactFile.exists()) {
            StringBuilder sbContact = new StringBuilder();

            try (Scanner scanner = new Scanner(new FileInputStream(contactFile))) {
                while (scanner.hasNextLine()) {
                    sbContact.append(scanner.nextLine()).append("\n");
                }
            } catch (IOException ex) {
                Logger.getLogger(PhoneBook.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            String contactInfo = sbContact.toString();
            
            return contactInfo;
        }

        return null;
    }

}
