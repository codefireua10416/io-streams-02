/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaphonebook;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        PhoneBook phoneBook = new PhoneBook("contacts");

        while (true) {
            System.out.println("ACTIONS");
            System.out.println("1. [A]dd new contact");
            System.out.println("2. [S]how all contacts");
            System.out.println("3. [G]et contact by id");

            String choose = scanner.nextLine().toLowerCase();

            switch (choose) {
                case "1":
                case "a":
                    System.out.println("Enter data (* - required):");

                    String fullName = input(scanner, "* Enter full name (min 3 symbols): ", true);
                    String phoneNumber = input(scanner, "* Enter phone number (+381234567): ", true);
                    String emailAddress = input(scanner, "Enter e-mail address: ", false);
                    String skype = input(scanner, "Enter skype: ", false);

                    phoneBook.addContact(fullName, phoneNumber, emailAddress, skype);
                    break;
                case "2":
                case "s":
                    List<String> nameList = phoneBook.getContactList();

                    System.out.println("All contacts:");
                    for (String name : nameList) {
                        System.out.println(" " + name);
                    }
                    break;
                case "3":
                case "g":

                    String contact_id = input(scanner, "Enter contact id: ", false);

                    String contact_info = phoneBook.getContact(contact_id);

                    if (contact_info == null || contact_info.isEmpty()) {
                        System.out.println("Contact was not found!");
                    } else {
                        System.out.println("Found:");
                        System.out.println(contact_info);
                    }

                    break;
            }

        }
    }

    private static String input(Scanner scanner, String prompt, Boolean required) {
        if (required) {
            while (true) {
                System.out.print(prompt);
                String input = scanner.nextLine();

                if (!input.isEmpty()) {
                    return input;
                }
            }
        } else {
            System.out.print(prompt);
            String input = scanner.nextLine();

            return input;
        }
    }

}
