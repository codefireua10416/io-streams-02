/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaphonebooktask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author human
 */
public class ContactBook implements PhoneBook, Iterable<Contact> {

    private List<Contact> contactList = new ArrayList<>();

    public List<Contact> getContactList() {
        return contactList;
    }

    @Override
    public long addContact(String fullName, String phoneNumber, String emailAddress, String skype) {
        long genId = System.currentTimeMillis();

        if (!contactList.isEmpty()) {
//            for (int i = 0; i < contactList.size(); i++) {
//                Contact contact = contactList.get(i);
//
//                if (contact.getId() == genId) {
//                    genId++;
//                    i = -1;
//                    System.out.println("reset loop");
//                }
//            }

            for (Contact contact : contactList) {
                if (contact.getId() >= genId) {
                    genId = contact.getId() + 1;
                }
            }
        }

        Contact contact = new Contact(genId, fullName, phoneNumber, emailAddress, skype);

        contactList.add(contact);

        return contact.getId();
    }

    @Override
    public Contact getById(long id) {
        for (Contact contact : contactList) {
            if (contact.getId() == id) {
                return contact;
            }
        }

        return null;
    }

    @Override
    public List<Contact> findByName(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Contact removeContact(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Iterator<Contact> iterator() {
        return contactList.iterator();
    }

}
